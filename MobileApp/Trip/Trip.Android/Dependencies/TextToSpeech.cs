﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Speech.Tts;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Trip.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(Trip.Droid.Dependencies.TextToSpeech))]
namespace Trip.Droid.Dependencies
{
    public class TextToSpeech :
        Java.Lang.Object, ITextToSpeech, Android.Speech.Tts.TextToSpeech.IOnInitListener
    {
        private Android.Speech.Tts.TextToSpeech speaker;
        private string textToRead;

        public void ReadText(string text)
        {
            textToRead = text;
            if (speaker == null)
            {
                speaker = new Android.Speech.Tts.TextToSpeech(MainActivity.Instance, this);
            }
            else
            {
                speaker.Speak(textToRead, QueueMode.Flush, null, null);
            }
        }

        public void OnInit(OperationResult status)
        {
            if (status.Equals(OperationResult.Success))
            {
                speaker.Speak(textToRead, QueueMode.Flush, null, null);
            }
        }
    }
}