﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace Trip.Converters
{
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var temporaryTime = (DateTime)value;
            return string.Format(culture, "{0:D2}/{1:D2}/{2:D2}", temporaryTime.Year, temporaryTime.Month, temporaryTime.Day);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
