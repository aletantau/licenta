﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Trip.Enums;
using Xamarin.Forms;

namespace Trip.Converters
{
    public class MenuIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = (MenuItemType)value;

            switch (type)
            {
                case MenuItemType.Cities:
                    return "ic_home.png";
                //case MenuItemType.TouristAttractions:
                //    return "ic_touristAttractions.png";
                case MenuItemType.Routes:
                    return "ic_routes.png";
                case MenuItemType.Profile:
                    return "ic_profile.png";
                case MenuItemType.Contact:
                    return "ic_contact.png";
                case MenuItemType.Logout:
                    return "ic_logout.png";
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
