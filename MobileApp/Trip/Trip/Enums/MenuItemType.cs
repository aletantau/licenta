﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trip.Enums
{
    public enum MenuItemType
    {
        Cities,
        Profile,
        TouristAttractions,
        Routes,
        Contact,
        Logout
    }
}
