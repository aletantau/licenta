﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Trip.ViewModels
{
    public class CreateAttractionViewModel : BaseViewModel
    {
        private ITouristAttractionsDataService touristAttractionsDataService;
        private Position position;
        private int selectedCityId;
        private string name;
        private string price;
        private string description;
        private string approxTime;
        private string note;

        public CreateAttractionViewModel(ITouristAttractionsDataService touristAttractions, INavigationService navigation, IDialogService dialog) : base(navigation, dialog)
        {
            this.touristAttractionsDataService = touristAttractions;
        }

        public int SelectedCityId
        {
            get => selectedCityId;
            set
            {
                selectedCityId = value;
                RaisePropertyChanged(() => SelectedCityId);
            }
        }

        public Position Position
        {
            get => position;
            set
            {
                position = value;
                RaisePropertyChanged(() => Position);
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Price
        {
            get => price;
            set
            {
                price = value;
                RaisePropertyChanged(() => Price);
            }
        }

        public string Description
        {
            get => description;
            set
            {
                description = value;
                RaisePropertyChanged(() => Description);
            }
        }
        public string ApproxTime
        {
            get => approxTime;
            set
            {
                approxTime = value;
                RaisePropertyChanged(() => ApproxTime);
            }
        }

        public string Note
        {
            get => note;
            set
            {
                note = value;
                RaisePropertyChanged(() => Note);
            }
        }

        public ICommand CreateAttraction => new Command(OnCreateAttraction);
        private async void OnCreateAttraction()
        {
            TouristAttraction attractionToCreate = new TouristAttraction() { Name = Name, Price = Convert.ToDecimal(Price), Description = Description, ApproxTime = Convert.ToDateTime(ApproxTime), Note = Note, Latitude = Position.Latitude, Longitude = Position.Longitude };
            var response = await touristAttractionsDataService.AddTouristAttraction(SelectedCityId,attractionToCreate);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet. Attraction Not updated.", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Attraction succesfully created", "Success", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

        public int GetCityId(Position position)
        {
            return 1;
        }
        public override async Task InitializeAsync(object position, object cityId)
        {
            this.Position = (Position)position;
            this.SelectedCityId = (int)cityId;
            await Task.Delay(1);
        }

    }
}
