﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class CityDetailViewModel : BaseViewModel
    {
        public ICityDataService citiesDataService;
        private City selectedCity;
        private string description;
        private string climate;
        private string note;

        public CityDetailViewModel(ICityDataService citiesDataService,INavigationService navigationService,
            IDialogService dialogService) : base(navigationService, dialogService)
        {
            this.citiesDataService=citiesDataService;
        }

        public City SelectedCity
        {
            get => selectedCity;
            set
            {
                selectedCity = value;
                RaisePropertyChanged(() => SelectedCity);
            }
        }

        public string Description
        {
            get => description;
            set
            {
                if(description!= value)
                {
                    description = value;
                    RaisePropertyChanged(() => Description);
                }
            }
        }

        public string Note
        {
            get => note;
            set
            {
                if (note != value)
                {
                    note = value;
                    RaisePropertyChanged(() => Note);
                }
            }
        }

        public string Climate
        {
            get => climate;
            set
            {
                if (climate != value)
                {
                    climate = value;
                    RaisePropertyChanged(() => Climate);
                }
            }
        }

        public override async Task InitializeAsync(object cityData, object data2 = null)
        {
            this.SelectedCity = await citiesDataService.GetCityAsync((City)cityData);
            if(SelectedCity == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
        }

        public ICommand AttractionTappedCommand => new Command<TouristAttraction>(OnAttractionTapped);
        private void OnAttractionTapped(TouristAttraction selectedAttraction)
        {
            NavigationService.NavigateToAsync<TouristAttractionViewModel>(selectedAttraction);
        }

        public ICommand GoToMapCommand => new Command(OnGoToMapClicked);
        public async void OnGoToMapClicked()
        {
            await NavigationService.NavigateToAsync<MapViewModel>(selectedCity);
        }

        public ICommand UpdateCommand => new Command(OnUpdate);
        private async void OnUpdate()
        {
            var city = new City() { Id=SelectedCity.Id , Description=Description, Climate=Climate, Note=Note, Name=SelectedCity.Name, ImageUrl=SelectedCity.ImageUrl, Latitude=SelectedCity.Latitude, Longitude=SelectedCity.Longitude};
            var response = await citiesDataService.UpdateCity(city);
            if(response==null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Successfully updated", "Update", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

    }
}
