﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Trip.Views;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IAuthenticationDataService _authenticationService;
        private string userName;
        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public ICommand SignInCommand => new Command(OnLogIn);
        public ICommand RegisterCommand => new Command(OnRegister);

        public LoginViewModel(IAuthenticationDataService authenticationService, INavigationService navigationService, IDialogService dialogService) : base(navigationService, dialogService)
        {
            _authenticationService = authenticationService;
        }

        public async void OnLogIn()
        {
            this.IsBusy = true;

            if (String.IsNullOrEmpty(UserName) || String.IsNullOrEmpty(Password))
            {
                this.IsBusy = false;
                DialogService.ShowToast(
                    "Please enter your username and password",
                    Color.Red);
                return;
            }


            var authenticationResponse = await _authenticationService.AuthenticateAsync(UserName, Password);
            if(authenticationResponse == null)
            {
                this.IsBusy = false;
                await DialogService.ShowDialog("Please connect to the internet", "No internet connection", "OK");
                return;
            }

            if (authenticationResponse.IsAuthenticated == false && authenticationResponse.User==null)
            {
                this.IsBusy = false;
                await DialogService.ShowDialog(
                    "Invalid username or password",
                    "Error logging you in",
                    "OK");
                return;
            }
            else
            {
                Globals.LoggedInUser = authenticationResponse.User;
                IsBusy = false;
                await NavigationService.NavigateToAsync<MainViewModel>();

            }
        }

        public void OnRegister()
        {
             NavigationService.NavigateToAsync<RegistrationViewModel>();
        }
    }
}
