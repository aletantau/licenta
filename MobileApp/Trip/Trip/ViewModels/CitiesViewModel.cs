﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Extensions;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class CitiesViewModel : BaseViewModel
    {
        private readonly ICityDataService citiesDataService;
        private ObservableCollection<City> cities;
        public CitiesViewModel(ICityDataService citiesDataService, INavigationService navigationService,
            IDialogService dialogService) : base(navigationService, dialogService)
        {
            this.citiesDataService = citiesDataService;

            Cities = new ObservableCollection<City>();
        }

        public ICommand CityTappedCommand => new Command<City>(OnCityTapped);

        public ObservableCollection<City> Cities
        {
            get => cities;
            set
            {
                cities = value;
                RaisePropertyChanged(() => Cities);
            }
        }

        private void OnCityTapped(City selectedCity)
        {
            NavigationService.NavigateToAsync<CityDetailViewModel>(selectedCity);
        }

        public override async Task InitializeAsync(object data, object data2=null)
        {
            this.Cities = (await citiesDataService.GetAllCitiesAsync()).ToObservableCollection();
            if(Cities==null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                return;
            }
        }
    }
}
