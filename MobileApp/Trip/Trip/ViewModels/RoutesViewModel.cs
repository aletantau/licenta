﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Extensions;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class RoutesViewModel : BaseViewModel
    {
        private readonly IRoutesDataService routesDataService;
        private ObservableCollection<Route> routes;
        private string name;

        public RoutesViewModel(IRoutesDataService routesDataService, INavigationService navigationService,
            IDialogService dialogService) : base(navigationService, dialogService)
        {
            this.routesDataService = routesDataService;

            Routes = new ObservableCollection<Route>();
        }

        public ICommand RouteTappedCommand => new Command<Route>(OnRouteTapped);

        public ObservableCollection<Route> Routes
        {
            get => routes;
            set
            {
                routes = value;
                RaisePropertyChanged(() => Routes);
            }
        }

        public string Name
        {
            get => name;
            set
            {
                if(name!=value)
                {
                    name = value;
                    RaisePropertyChanged(() => Name);
                }
            }
        }

        private void OnRouteTapped(Route selectedRoute)
        {
            NavigationService.NavigateToAsync<RouteDetailViewModel>(selectedRoute);
        }


        public override async Task InitializeAsync(object navigationData, object data2 = null)
        {
            this.Routes = (await routesDataService.GetAllRoutesForUserAsync())?.ToObservableCollection();
            if (Routes == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
        }

        public ICommand CreateRouteCommand => new Command(OnCreateRoute);
        private async void OnCreateRoute()
        {
           await NavigationService.NavigateToAsync<CreateRouteViewModel>();
        }

        public ICommand RouteNameChangedCommand => new Command<Route>(OnRouteNameChanged);
        private async void OnRouteNameChanged(Route route)
        {
            var response = await routesDataService.UpdateRoute(route);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                //await NavigationService.NavigateToAsync<MainViewModel>();
            }
        }
    }
}
