﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Interfaces;
using Trip.Models;

namespace Trip.ViewModels
{
    public class ProfileViewModel: BaseViewModel
    {
        private User user;
        public User User
        {
            get => user;
            set
            {
                user = value;
                RaisePropertyChanged(() => User);
            }
        }

        private string fullName;
        public string FullName
        {
            get => fullName;
            set
            {
                fullName = value;
                RaisePropertyChanged(() => FullName);
            }
        }
        public ProfileViewModel(INavigationService navigationService,
            IDialogService dialogService) : base(navigationService, dialogService)
        {
        }

        public override async Task InitializeAsync(object navigationData, object data2 = null)
        {
            this.User = Globals.LoggedInUser;
            this.FullName = Globals.LoggedInUser.FirstName + " " + Globals.LoggedInUser.LastName;

            await Task.Delay(1);
        }
    }
}
