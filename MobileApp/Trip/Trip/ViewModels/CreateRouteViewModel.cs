﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Extensions;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class CreateRouteViewModel : BaseViewModel
    {
        private readonly IRoutesDataService routesDataService;
        private Route route;
        private string name;
        private ObservableCollection<Route> routes;
        public CreateRouteViewModel(IRoutesDataService routesData, INavigationService navigation, IDialogService dialog) : base(navigation,dialog)
        {
            this.routesDataService = routesData;
            Routes = new ObservableCollection<Route>();
        }

        public ObservableCollection<Route> Routes
        {
            get => routes;
            set
            {
                routes = value;
                RaisePropertyChanged(() => Routes);
            }
        }

        public Route Route
        {
            get => route;
            set
            {
                route = value;
                RaisePropertyChanged(() => Route);
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public override async Task InitializeAsync(object data, object data2 = null)
        {
            this.Routes = (await routesDataService.GetAllRoutesForUserAsync()).ToObservableCollection();
            if (Routes == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
        }

        public ICommand CreateRouteCommand => new Command(async () => { await OnCreateRoute(); });
        private async Task OnCreateRoute()
        {
            if(this.Routes.Where(r=>r.Name== Name).Any() == true)
            {
                await DialogService.ShowDialog($"A Route with name {Name} already exists", "Not Created", "OK");
                return;
            }
            else
            {
                this.Route = new Route() { Name = Name };
                var response = await routesDataService.AddRoute(route);
                if (response == null)
                {
                    await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                    await NavigationService.NavigateToAsync<MainViewModel>();
                }
                await DialogService.ShowDialog("Route succesfully created", "Create Route", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
                //await NavigationService.NavigateBackAsync();
            }

        }
    }
}
