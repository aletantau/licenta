﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Models;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.GoogleMaps.Bindings;

namespace Trip.ViewModels
{
    public class MapViewModel : BaseViewModel
    {
        public MapViewModel(INavigationService navigation, IDialogService dialog) : base(navigation, dialog)
        {
        }

        //the default visible region when you open the maps
        private MapSpan visibleRegion;
        public MapSpan VisibleRegion
        {
            get => visibleRegion;
            set
            {
                visibleRegion = value;
                RaisePropertyChanged(() => VisibleRegion);
            }
        }

        //a collection of all the pins required
        private ObservableCollection<Pin> pins;
        public ObservableCollection<Pin> Pins
        {
            get => pins;
            set
            {
                pins = value;
                RaisePropertyChanged(() => Pins);
            }
        }
        private City selectedCity;
        public City SelectedCity
        {
            get => selectedCity;
            set
            {
                selectedCity = value;
                RaisePropertyChanged(() => SelectedCity);
            }
        }

        private IList<TouristAttraction> attractions;
        public IList<TouristAttraction> SelectedAttractions
        {
            get => attractions;
            set
            {
                attractions = value;
                RaisePropertyChanged(() => SelectedAttractions);
            }
        }

        private ObservableCollection<Position> positions;
        public ObservableCollection<Position> Positions
        {
            get => positions;
            set
            {
                positions = value;
                RaisePropertyChanged(() => Positions);
            }
        }

        //Adding all the Positions(Latitude and Longitude) from the SelectedAttractions
        private void InitializePositions()
        {
            Positions = new ObservableCollection<Position>();
            foreach(var attraction in SelectedAttractions)
            {
                var pos = new Position(attraction.Latitude, attraction.Longitude);
                Positions.Add(pos);
            }
            RaisePropertyChanged(() => Positions);
        }

        public MoveToRegionRequest MoveToRegionRequest { get; } = new MoveToRegionRequest();

        private ObservableCollection<Polyline> routeLines;
        public ObservableCollection<Polyline> RouteLinesPoints
        {
            get => routeLines;
            set
            {
                routeLines = value;
                RaisePropertyChanged(() => RouteLinesPoints);
            }
        }

        public override async Task InitializeAsync(object cityData, object data2 = null)
        {
            this.SelectedCity = (City)cityData;
            this.SelectedAttractions = (List<TouristAttraction>)SelectedCity.Attractions;
            RouteLinesPoints = new ObservableCollection<Polyline>();

            InitializePositions();
            VisibleRegion = MapSpan.FromPositions(Positions);
            MoveToRegionRequest.MoveToRegion(VisibleRegion);

            Polyline routeLine = new Polyline
            {
                StrokeColor = Xamarin.Forms.Color.Black,
                StrokeWidth = 3f
            };

            foreach (TouristAttraction attraction in SelectedAttractions)
            {
                var pos = new Position(attraction.Latitude, attraction.Longitude);
                routeLine.Positions.Add(pos);

                var pin = new Pin()
                {
                    Type = PinType.Place,
                    Address = "The address",
                    Label = attraction.Name,
                    Position = pos
                };
                Pins.Add(pin);
            }

            RouteLinesPoints.Add(routeLine);
            RaisePropertyChanged(() => Pins);
            RaisePropertyChanged(() => RouteLinesPoints);
            await Task.Delay(1);
        }

        public ICommand CreateAttractionCommand => new Command<MapClickedEventArgs>(async (args1) => await OnCreateAttraction(args1,SelectedCity.Id));
        private async Task OnCreateAttraction(MapClickedEventArgs args, int cityId)
        {
            Position attractionPosition = new Position(args.Point.Latitude, args.Point.Longitude);
            await NavigationService.NavigateToAsync<CreateAttractionViewModel>(attractionPosition,cityId);
        }

        public ICommand PinClickedCommand => new Command<PinClickedEventArgs>(async (args) => await ShowPinDetails(args));
        private async Task ShowPinDetails(PinClickedEventArgs args)
        {
            var tempAttraction = GetAttraction(args.Pin.Position);
            if(tempAttraction!=null)
            {
                await NavigationService.NavigateToAsync<TouristAttractionViewModel>(tempAttraction);
            }
            else
            {
                await DialogService.ShowDialog("Something went wrong. Attraction not available", "Error", "OK");
            }
        }

        private TouristAttraction GetAttraction(Position position)
        {
            TouristAttraction attractionToReturn = null;
            foreach(var attraction in SelectedAttractions)
            {
                if(attraction.Latitude==position.Latitude && attraction.Longitude==position.Longitude)
                {
                    attractionToReturn=attraction;
                    break;
                }
            }
            return attractionToReturn;
        }
    }
}
