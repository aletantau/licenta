﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Trip.Enums;
using Trip.Interfaces;
using Trip.Models;
using Xamarin.Forms;
using MenuItem = Trip.Models.MenuItem;

namespace Trip.ViewModels
{
    public class MenuViewModel : BaseViewModel
    {
        public ICommand MenuItemTappedCommand => new Command(SelectMenuItemAsync);

        private ObservableCollection<MenuItem> _menuItems;
        public ObservableCollection<MenuItem> MenuItems
        {
            get => _menuItems;
            set
            {
                _menuItems = value;
                RaisePropertyChanged(() => MenuItems);
            }
        }


        private User _currentUser;
        public User CurrentUser
        {
            get => _currentUser;
            set
            {
                _currentUser = value;
                RaisePropertyChanged(() => CurrentUser);
            }
        }


        public MenuViewModel(INavigationService navigationService,
            IDialogService dialogService): base(navigationService, dialogService)
        {
            CurrentUser = Globals.LoggedInUser;
            _menuItems = new ObservableCollection<MenuItem>();
            InitMenuItems();
        }


        private void InitMenuItems()
        {
            MenuItems.Add(new MenuItem
            {
                Title = "Cities",
                MenuItemType = MenuItemType.Cities,
                ViewModelType = typeof(CitiesViewModel)
                //IsEnabled = true
            });

            MenuItems.Add(new MenuItem
            {
                Title = "Profile",
                MenuItemType = MenuItemType.Profile,
                ViewModelType = typeof(ProfileViewModel)
                //IsEnabled = true
            });
                        
            MenuItems.Add(new MenuItem
            {
                Title = "Routes",
                MenuItemType = MenuItemType.Routes,
                ViewModelType = typeof(RoutesViewModel)
            });

            MenuItems.Add(new MenuItem
            {
                Title = "Contact us",
                MenuItemType = MenuItemType.Contact,
                ViewModelType = typeof(ContactViewModel)
            });

            MenuItems.Add(new MenuItem
            {
                Title = "Log out",
                MenuItemType = MenuItemType.Logout,
                ViewModelType = typeof(LoginViewModel)
                //IsEnabled = true
            });

        }


        private void SelectMenuItemAsync(object item)
        {
            var menuItem = ((item as ItemTappedEventArgs)?.Item as MenuItem);

            if (menuItem != null && menuItem.Title == "Log out")
            {
                Globals.LoggedInUser = null;
                NavigationService.ClearBackStack();
            }

            var type = menuItem?.ViewModelType;
            NavigationService.NavigateToAsync(type);

        }

    }
}