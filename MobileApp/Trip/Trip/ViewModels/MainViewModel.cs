﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Interfaces;

namespace Trip.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private MenuViewModel _menuViewModel;
        public MenuViewModel MenuViewModel
        {
            get => _menuViewModel;
            set
            {
                _menuViewModel = value;
                RaisePropertyChanged(() => MenuViewModel);
            }
        }

        public MainViewModel(MenuViewModel menuViewModel, INavigationService navigationService,
            IDialogService dialogService): base(navigationService, dialogService)
        {
            _menuViewModel = menuViewModel;
        }

        public override Task InitializeAsync(object navigationData, object obj2=null)
        {
            return Task.WhenAll
            (
                _menuViewModel.InitializeAsync(navigationData),
                NavigationService.NavigateToAsync<CitiesViewModel>()
            );
        }
    }
}
