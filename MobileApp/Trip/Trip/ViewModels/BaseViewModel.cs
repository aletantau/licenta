﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Trip.Annotations;
using Trip.Interfaces;
using Trip.Utility;

namespace Trip.ViewModels
{
    public class BaseViewModel : ExtendedBindableObject
    {
        private bool isBusy;
        protected readonly IDialogService DialogService;
        protected readonly INavigationService NavigationService;
        public BaseViewModel(INavigationService navigationService,
            IDialogService dialogService)
        {
            DialogService = dialogService;
            NavigationService = navigationService;
        }
        
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public virtual Task InitializeAsync(object data)
        {
            return Task.FromResult(false);
        }

        public virtual Task InitializeAsync(object data1, object data2)
        {
            return Task.FromResult(false);
        }

    }
}
