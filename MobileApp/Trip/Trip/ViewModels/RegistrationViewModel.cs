﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Trip.Views;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class RegistrationViewModel : BaseViewModel
    {
        private readonly IAuthenticationDataService authenticationService;
        private string userName;
        private string firstName;
        private string lastName;
        private string password;
        private string email;

        public RegistrationViewModel(IAuthenticationDataService authentication, INavigationService navigationService,
            IDialogService dialogService): base(navigationService, dialogService)
        {
            this.authenticationService = authentication;
        }
        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        public string FirstName
        {
            get => firstName;
            set
            {
                firstName = value;
                RaisePropertyChanged(() => FirstName);
            }
        }

        public string LastName
        {
            get => lastName;
            set
            {
                lastName = value;
                RaisePropertyChanged(() => LastName);
            }
        }

        public string Password
        {
            get => password;
            set
            {
                password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public string Email
        {
            get => email;
            set
            {
                email = value;
                RaisePropertyChanged(() => Email);
            }
        }

        public ICommand RegisterCommand => new Command(RegisterAsync);
        public ICommand SigninCommand => new Command(OnLogin);

        public async void RegisterAsync()
        {
            this.IsBusy = true;
            var userRegistered = await authenticationService.RegisterAsync(this.UserName, this.Password, this.FirstName, this.LastName, this.Email);

            if (userRegistered == null)
            {
                this.IsBusy = false;
                await DialogService.ShowDialog("Please connect to the internet", "No internet connection", "OK");
                return;
            }

            if (userRegistered.IsAuthenticated == false && userRegistered.User == null)
            {
                this.IsBusy = false;
                DialogService.ShowToast("This username is already used", Color.Red);
                return;
            }

            if (userRegistered.IsAuthenticated == false && userRegistered.User!=null)
            {
                this.IsBusy = false;
                await DialogService.ShowDialog("Successfully registrated", "Registraction", "OK");
                await NavigationService.NavigateToAsync<LoginViewModel>();
            }
        }

        public void OnLogin()
        {
            NavigationService.NavigateToAsync<LoginViewModel>();
        }

    }
}
