﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Extensions;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class TouristAttractionViewModel : BaseViewModel
    {
        private ITouristAttractionsDataService attractionsDataService;
        private IRoutesDataService routesDataService;
        private TouristAttraction selectedAttraction;
        public ObservableCollection<Route> routes;
        private string address;
        private string name;
        private string description;
        private string approxTime;
        private string note;
        private string price;

        public TouristAttractionViewModel(ITouristAttractionsDataService attractionsDataService, IRoutesDataService routesDataService, INavigationService navigationService, IDialogService dialogService) : base(navigationService, dialogService)
        {
            this.attractionsDataService = attractionsDataService;
            this.routesDataService = routesDataService;
            Routes = new ObservableCollection<Route>();
        }

        public TouristAttraction SelectedAttraction
        {
            get => selectedAttraction;
            set
            {
                selectedAttraction = value;
                RaisePropertyChanged(() => SelectedAttraction);
            }
        }

        public string Name
        {
            get => name;
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged(() => Name);
                }
            }
        }
        public string Description
        {
            get => description;
            set
            {
                if (description != value)
                {
                    description = value;
                    RaisePropertyChanged(() => Description);
                }
            }
        }

        public string Price
        {
            get => price;
            set
            {
                if (price != value)
                {
                    price = value;
                    RaisePropertyChanged(() => Price);
                }
            }
        }

        public string ApproxTime
        {
            get => approxTime;
            set
            {
                if (approxTime != value)
                {
                    approxTime = value;
                    RaisePropertyChanged(() => ApproxTime);
                }
            }
        }

        public string Note
        {
            get => note;
            set
            {
                if (note != value)
                {
                    note = value;
                    RaisePropertyChanged(() => Note);
                }
            }
        }

        public string Address
        {
            get => address; 
            set
            {
                if (address != value)
                {
                    address = value;
                    RaisePropertyChanged(() => Address);
                }
            }
        }

        public ObservableCollection<Route> Routes
        {
            get => routes;
            set
            {
                routes = value;
                RaisePropertyChanged(() => Routes);
            }
        }
        private string ParseDateTime(DateTime date)
        {
            return string.Format("{0:D2}:{1:D2}:{2:D2}", date.Hour, date.Minute, date.Second);
        }
        private string ParsePrice(decimal price)
        {
            return string.Format("${0}", price);
        }
        private decimal InverseParsePrice(string price)
        {
            string temporaryPrice = price.Split('$')[1];
            return Convert.ToDecimal(temporaryPrice);
        }
        private DateTime InverseParseApproxtime(string dateTime)
        {
            var givenTime = dateTime.Split(':');
            if( givenTime.Length != 3)
            {
                DialogService.ShowDialog("Please give a valid date time format(hh:mm:ss)", "Invalid input", "OK");
                NavigationService.NavigateBackAsync();
            }
            return new DateTime(1, 1, 1, Convert.ToInt32(givenTime[0]), Convert.ToInt32(givenTime[1]), Convert.ToInt32(givenTime[2]));
        }
        public override async Task InitializeAsync(object attraction, object data2 = null)
        {
            this.SelectedAttraction = (TouristAttraction)attraction;
            this.Address = SelectedAttraction.Address;
            this.Price = ParsePrice(SelectedAttraction.Price);
            this.Description = SelectedAttraction.Description;
            this.Note = SelectedAttraction.Note;
            this.ApproxTime = ParseDateTime(SelectedAttraction.ApproxTime);
            this.Name = SelectedAttraction.Name;
            this.Routes = (await routesDataService.GetAllRoutesForUserAsync())?.ToObservableCollection();
        }

        private bool CompareAttractions(TouristAttraction attraction1, TouristAttraction attraction2)
        {
            bool areEqual = false;
            if (attraction1.Id == attraction2.Id && attraction1.Latitude == attraction2.Latitude && attraction1.Longitude == attraction2.Longitude &&
                attraction1.Name == attraction2.Name && attraction1.ImageUrl == attraction2.ImageUrl && attraction1.Note == attraction2.Note && attraction1.Description == attraction2.Description)
            {
                areEqual = true;
            }
            return areEqual;
        }

        public ICommand AddToRouteCommand => new Command(OnAddToRouteClicked);
        private async void OnAddToRouteClicked()
        {
            string routeNames = string.Join(",", Routes.Select((route) => { return route.Name; }));
            string action = await DialogService.DisplayActionSheet("Route: Add to?", "Cancel", null, null, routeNames.Split(','));
            //get the id from the routes with the name from the resulted action
            int routeId = 0;
            foreach (var route in Routes)
            {
                if (route.Name == action)
                {
                    routeId = route.Id;
                    TouristAttraction attractionToCompar = route.Attractions.Where(a => CompareAttractions(a, SelectedAttraction) == true).FirstOrDefault();
                    if (attractionToCompar != null)
                    {
                        await DialogService.ShowDialog($"{SelectedAttraction.Name} already exists in {route.Name}", "Not Added", "OK");
                        return;
                    }
                    break;
                }
            }
            var response = await routesDataService.AddAttractionAsync(SelectedAttraction, routeId);
            if(response== null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog($"Attraction added to {action}", "Success", "Ok");
        }

        public ICommand ReadDescriptionCommand => new Command(OnReadDescription);
        private async void OnReadDescription()
        {
            DependencyService.Get<ITextToSpeech>().ReadText(SelectedAttraction.Description);
            await Task.Delay(1);
        }

        public ICommand DeleteCommand => new Command(async () => await OnDelete());
        private async Task OnDelete()
        {
            var response = await attractionsDataService.DeleteAttraction(SelectedAttraction);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet. Attraction Not updated.", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Successfully deleted", "Delete", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

        public ICommand UpdateCommand => new Command(OnUpdate);
        private async void OnUpdate()
        {
            var attraction = new TouristAttraction() { Id = SelectedAttraction.Id, Name = Name, Address = Address, Price = InverseParsePrice(Price), Description = Description, ApproxTime = InverseParseApproxtime(ApproxTime), ImageUrl = SelectedAttraction.ImageUrl, Latitude = SelectedAttraction.Latitude, Longitude = SelectedAttraction.Longitude, Note = Note };
            var response = await attractionsDataService.UpdateAttraction(attraction);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet. Attraction Not updated.", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Successfully updated", "Update", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }
    }
}
