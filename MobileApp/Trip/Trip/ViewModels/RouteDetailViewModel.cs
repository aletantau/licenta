﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class RouteDetailViewModel : BaseViewModel
    {
        private IRoutesDataService routesDataService;
        private Route selectedRoute;
        public RouteDetailViewModel(IRoutesDataService routesDataService, INavigationService navigationService,
            IDialogService dialogService) : base(navigationService, dialogService)
        {
            this.routesDataService = routesDataService;
        }

        public Route SelectedRoute
        {
            get => selectedRoute;
            set
            {
                selectedRoute = value;
                RaisePropertyChanged(() => SelectedRoute);
            }
        }
        public override async Task InitializeAsync(object route, object data2 = null)
        {
            this.SelectedRoute = await routesDataService.GetRouteAsync((Route)route);
            if (SelectedRoute == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
        }
       
        public ICommand AttractionTappedCommand => new Command<TouristAttraction>(OnAttractionTapped);

        private void OnAttractionTapped(TouristAttraction selectedAttraction)
        {
            NavigationService.NavigateToAsync<TouristAttractionViewModel>(selectedAttraction);
        }

        public ICommand GoToMapCommand => new Command(OnGoToMapClicked);
        public async void OnGoToMapClicked()
        {
            await NavigationService.NavigateToAsync<MapViewModel>(selectedRoute.Attractions.ToList());
        }

        public ICommand DeleteCommand => new Command(async () => await OnDelete());
        private async Task OnDelete()
        {
            var response = await routesDataService.DeleteRoute(SelectedRoute);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Successfully deleted", "Delete", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

        public ICommand DeleteAttractionCommand => new Command<TouristAttraction>(OnDeleteAttraction);
        private async void OnDeleteAttraction(TouristAttraction attraction)
        {
            var response = await routesDataService.DeleteAttractionFromRoute(attraction, SelectedRoute.Id);
            if (response == null)
            {
                await DialogService.ShowDialog("Please connect to the internet", " No internet connection", "OK");
                await NavigationService.NavigateToAsync<MainViewModel>();
            }
            await DialogService.ShowDialog("Successfully deleted", "Delete", "OK");
            await NavigationService.NavigateToAsync<MainViewModel>();
        }
    }
}
