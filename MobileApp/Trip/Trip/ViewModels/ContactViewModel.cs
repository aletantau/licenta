﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trip.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Trip.ViewModels
{
    public class ContactViewModel : BaseViewModel
    {
        private readonly IPhoneService phoneService;
        public ContactViewModel(INavigationService navigationService, IDialogService dialogService,
          IPhoneService phoneService)
           : base(navigationService, dialogService)
        {
            this.phoneService = phoneService;
        }

        public ICommand CallPhone => new Command(OnCallPhone);
        private void OnCallPhone()
        {
            phoneService.MakePhoneCall();
        }

        public ICommand SendEmailCommand => new Command(OnSendEmail);
        private async void OnSendEmail()
        {
            try
            {
                var message = new EmailMessage
                {
                    Subject = null,
                    Body = null,
                    To = new List<string>() { "tantau_alexandra@yahoo.com" },
                };
                await Email.ComposeAsync(message);
            }
            //catch (FeatureNotSupportedException fbsEx)
            //{
            //    // Email is not supported on this device  
            //}
            catch (Exception ex)
            {
                // Some other exception occurred  
            }
        }
    }
}
