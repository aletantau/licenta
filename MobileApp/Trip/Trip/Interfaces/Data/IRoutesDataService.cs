﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Models;

namespace Trip.Interfaces.Data
{
    public interface IRoutesDataService
    {
        Task<IEnumerable<Route>> GetAllRoutesForUserAsync();
        //Task<IEnumerable<Route>> GetAllRoutesForUserWithAttractionsAsync();
        Task<TouristAttraction> AddAttractionAsync(TouristAttraction touristAttraction,int routeId);
        Task<Route> AddRoute(Route route);
        Task<Route> GetRouteAsync(Route route);

        Task<Route> DeleteRoute(Route route);
        Task<TouristAttraction> DeleteAttractionFromRoute(TouristAttraction attraction, int routeId);
        Task<Route> UpdateRoute(Route route);
    }
}
