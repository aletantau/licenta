﻿
using System.Threading.Tasks;
using Trip.Models;

namespace Trip.Interfaces.Data
{
    public interface IAuthenticationDataService
    {
        Task<AuthenticationResponse> AuthenticateAsync(string UserName, string Password);
        Task<AuthenticationResponse> RegisterAsync(string UserName, string Password, string FirstName, string LastName, string Email);
        bool IsUserAuthenticated();
    }
}
