﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Models;

namespace Trip.Interfaces.Data
{
    public interface ITouristAttractionsDataService
    {
        Task<TouristAttraction> DeleteAttraction(TouristAttraction touristAttraction);
        Task<TouristAttraction> AddTouristAttraction(int cityId,TouristAttraction attraction);
        Task<TouristAttraction> UpdateAttraction(TouristAttraction attraction);
    }
}
