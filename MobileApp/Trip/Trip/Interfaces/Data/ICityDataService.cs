﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Models;

namespace Trip.Interfaces.Data
{
    public interface ICityDataService
    {
        Task<IEnumerable<City>> GetAllCitiesAsync();
        Task<City> GetCityAsync(City city);
        Task<City> UpdateCity(City city);
    }
}
