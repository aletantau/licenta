﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.ViewModels;

namespace Trip.Interfaces
{
    public interface INavigationService
    {
        Task InitializeAsync(bool isPushToStack = true);

        Task NavigateToAsync<TViewModel>(bool isPushToStack = true) where TViewModel : BaseViewModel;
        Task NavigateToAsync<TViewModel>(object parameter, bool isPushToStack = true) where TViewModel : BaseViewModel;
        Task NavigateToAsync<TViewModel>(object parameter1, object parameter2, bool isPushToStack = true) where TViewModel : BaseViewModel;

        Task NavigateToAsync(Type viewModelType, bool isPushToStack = true);

        Task ClearBackStack();

        Task NavigateToAsync(Type viewModelType, object parameter, bool isPushToStack = true);

        Task NavigateBackAsync();

        Task RemoveLastFromBackStackAsync();

        Task PopToRootAsync();

    }
}
