﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trip.Interfaces
{
    public interface IPhoneService
    {
        void MakePhoneCall();
    }
}
