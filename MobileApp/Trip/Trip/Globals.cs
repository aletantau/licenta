﻿using System;
using System.Collections.Generic;
using System.Text;
using Trip.Models;

namespace Trip
{
    public static class Globals
    {
        public static bool UseData = true;

        public static User LoggedInUser = null;
    }
}
