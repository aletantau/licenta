﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trip.Constants
{
    public class CacheNameConstants
    {
        public const string AllCities = "AllCities";
        public const string AllCitiesWithAttractions = "AllCities";

        public const string User = "User";

        public const string AllRoutes = "AllRoutes";
        public const string AllAttractions = "AllAttractions";
    }
}
