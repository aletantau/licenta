﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trip.Constants
{
    public class ApiConstants
    {
        public const string BaseApiUrl = "http://192.168.1.4:3280/";
        public const string CitiesEndpoint = "api/cities/";
        public const string AttractionsEndpoint = "api/touristAttractions";
        public const string RoutesEndpoint = "api/routes";
        public const string UpdateRouteEndpoint = "api/routes/update";
        public const string RoutesWithAttractionsEndpoint = "api/routes/withAttractions";
        public const string RouteDetailsEndpoint = "api/routes/routeDetails";
        public const string CreateRouteEndpoint = "api/routes/create";
        public const string AddAttractionToRouteEndpoint = "api/routes/addTA";
        public const string DeleteAttractionToRouteEndpoint = "api/routes/deleteAttraction";
        public const string AddAttractionToCityEndpoint = "api/touristAttractions/withCityId";
        public const string DeleteAttractionEndpoint = "api/touristAttractions";
        public const string RegisterEndpoint = "api/authentication/register";
        public const string AuthenticateEndpoint = "api/authentication/authenticate";
    }
}
