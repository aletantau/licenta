﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trip.Models
{
    public class TouristAttraction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime ApproxTime { get; set; }
        public string Note { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ImageUrl { get; set; }
    }
}
