﻿using System;
using System.Collections.Generic;
using System.Text;
using Trip.Enums;

namespace Trip.Models
{
    public class MenuItem
    {
        public string Title { get; set; }
        public MenuItemType MenuItemType { get; set; }
        public Type ViewModelType { get; set; }
        //public bool IsEnabled { get; set; }
    }
}
