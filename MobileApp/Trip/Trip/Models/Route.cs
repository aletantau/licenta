﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Trip.Models
{
    public class Route
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public int UserId { get; set; }
        public ObservableCollection<TouristAttraction> Attractions { get; set; } = new ObservableCollection<TouristAttraction>();
    }
}
