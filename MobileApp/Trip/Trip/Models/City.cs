﻿using System.Collections.Generic;

namespace Trip.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public string Climate { get; set; }
        public string Note { get; set; }
        public string ImageUrl { get; set; }
        //public int NumberOfAttractions => Attractions.Count;
        public ICollection<TouristAttraction> Attractions { get; set; } = new List<TouristAttraction>();
    }
}
