﻿using Autofac;
using System;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Repository;
using Trip.Services;
using Trip.Services.General;
using Trip.ViewModels;

namespace Trip.Bootstrap
{
    public class AppContainer
    {
        private static IContainer container;

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            //ViewModels
            builder.RegisterType<CitiesViewModel>();
            builder.RegisterType<MainViewModel>();
            builder.RegisterType<MenuViewModel>();
            builder.RegisterType<LoginViewModel>();
            builder.RegisterType<RegistrationViewModel>();
            builder.RegisterType<ProfileViewModel>();
            builder.RegisterType<CityDetailViewModel>();
            builder.RegisterType<TouristAttractionViewModel>();
            builder.RegisterType<RouteDetailViewModel>();
            builder.RegisterType<RoutesViewModel>();
            builder.RegisterType<MapViewModel>();
            builder.RegisterType<ContactViewModel>();
            builder.RegisterType<CreateRouteViewModel>();
            builder.RegisterType<CreateAttractionViewModel>();

            //services - data
            builder.RegisterType<CityDataService>().As<ICityDataService>();
            builder.RegisterType<RouteDataService>().As<IRoutesDataService>();
            builder.RegisterType<UserDataService>().As<IUserDataService>();
            builder.RegisterType<TouristAttractionsDataService>().As<ITouristAttractionsDataService>();

            //services - general
            builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<AuthenticationDataService>().As<IAuthenticationDataService>();
            builder.RegisterType<PhoneService>().As<IPhoneService>();


            //General
            builder.RegisterType<GenericRepository>().As<IGenericRepository>();

            container = builder.Build();
        }

        public static object Resolve(Type typeName)
        {
            return container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
