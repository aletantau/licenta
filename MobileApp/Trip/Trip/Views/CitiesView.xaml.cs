﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trip.Interfaces;
using Trip.Repository;
using Trip.Services;
using Trip.ViewModels;
using Xamarin.Forms;

namespace Trip.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class CitiesView : ContentPage
    {
        public CitiesView()
        {
            InitializeComponent();
        }
    }
}
