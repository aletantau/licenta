﻿using System;
using System.Threading.Tasks;
using Trip.Bootstrap;
using Trip.Interfaces;
using Trip.Utility;
using Trip.ViewModels;
using Trip.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Trip
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            InitializeApp();
            InitializeNavigation();
        }

        private void InitializeApp()
        {
           //if(Globals.UseData)
            AppContainer.RegisterDependencies();

        }

        private async Task InitializeNavigation()
        {
            var navigationService = AppContainer.Resolve<INavigationService>();
            await navigationService.InitializeAsync();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
