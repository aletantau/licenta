﻿using Akavache;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Trip.Constants;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Essentials;

namespace Trip.Services
{
    public class CityDataService : BaseService, ICityDataService
    {
        private readonly IGenericRepository _genericRepository;
    
        public CityDataService(IGenericRepository genericRepository,
            IBlobCache cache = null) : base(cache)
        {
            _genericRepository = genericRepository;
        }

        //Get all the Cities without their tourist attractions
        public async Task<IEnumerable<City>> GetAllCitiesAsync()
        {
            if(Connectivity.NetworkAccess == NetworkAccess.None)
            {
                List<City> citiesFromCache = await GetFromCache<List<City>>(CacheNameConstants.AllCities);

                if (citiesFromCache != null) //loaded from cache
                {
                    return citiesFromCache;
                }
                return null;
            }
            
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.CitiesEndpoint
            };   
                
            var cities = await _genericRepository.GetAsync<IEnumerable<City>>(builder.ToString());
            await Cache.InsertObject(CacheNameConstants.AllCities, cities, DateTimeOffset.Now.AddSeconds(90000000000));

            return cities;
        }

        //Get city with its tourist attractions
        public async Task<City> GetCityAsync(City city)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                List<City> citiesWithAttractionsFromCache = await GetFromCache<List<City>>(CacheNameConstants.AllCities);
                City cityReturn = citiesWithAttractionsFromCache?.Where(c => c.Id == city.Id).FirstOrDefault();
                if (citiesWithAttractionsFromCache != null && cityReturn != null && cityReturn.Attractions.Count != 0)//loaded from cache
                {
                    return cityReturn;
                }
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.CitiesEndpoint+$"{city.Id}"
            };
            var cityToReturn = await _genericRepository.GetAsync<City>(builder.ToString());

            return cityToReturn;
        }

        public async Task<City> UpdateCity(City city)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

             UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AttractionsEndpoint + $"/{city.Id}"
            };

            var response = await _genericRepository.PutAsync<City>(builder.ToString(), city);
            return response;
        }
    }
}
