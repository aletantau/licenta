﻿using Akavache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Constants;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Essentials;

namespace Trip.Services
{
    public class TouristAttractionsDataService : BaseService, ITouristAttractionsDataService
    {
        private readonly IGenericRepository _genericRepository;

        public TouristAttractionsDataService(IGenericRepository genericRepository,
            IBlobCache cache = null) : base(cache)
        {
            _genericRepository = genericRepository;
        }


        //Add tourist attraction to a city
        public async Task<TouristAttraction> AddTouristAttraction(int cityId, TouristAttraction attraction)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AddAttractionToCityEndpoint + $"/{cityId}"
            };

            await _genericRepository.PostAsync<TouristAttraction>(builder.ToString(),attraction);
            return attraction;
        }

        //Delete attraction
        public async Task<TouristAttraction> DeleteAttraction(TouristAttraction touristAttraction)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.DeleteAttractionEndpoint+$"/{touristAttraction.Id}"
            };

            await _genericRepository.DeleteAsync(builder.ToString());
            return touristAttraction;
        }

        //Update Tourist Attraction
        public async Task<TouristAttraction> UpdateAttraction(TouristAttraction attraction)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AttractionsEndpoint + $"/{attraction.Id}"
            };

            await _genericRepository.PutAsync<TouristAttraction>(builder.ToString(), attraction);
            return attraction;
        }

    }
}
