﻿using Akavache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Trip.Constants;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Essentials;

namespace Trip.Services
{
    public class RouteDataService : BaseService, IRoutesDataService
    {
        private readonly IGenericRepository genericRepository;
       
        public RouteDataService(IGenericRepository genericRepo,
           IBlobCache cache = null) : base(cache)
        {
            this.genericRepository = genericRepo;
        }

        public async Task<TouristAttraction> AddAttractionAsync(TouristAttraction touristAttraction, int routeId)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AddAttractionToRouteEndpoint + "/" + routeId.ToString()
            };

            await genericRepository.PostAsync(builder.ToString(), touristAttraction);
            
            return touristAttraction;
        }

        public async Task<IEnumerable<Route>> GetAllRoutesForUserAsync()
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                List<Route> routesFromCache = await GetFromCache<List<Route>>(CacheNameConstants.AllRoutes);

                if (routesFromCache != null) //loaded from cache
                {
                    return routesFromCache;
                }
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.RoutesWithAttractionsEndpoint + "/" + Globals.LoggedInUser.Id.ToString()
            };

            var routes = await genericRepository.GetAsync<List<Route>>(builder.ToString());
            await Cache.InsertObject(CacheNameConstants.AllRoutes, routes, DateTimeOffset.Now.AddSeconds(9000000));

            return routes;
        }

        public async Task<Route> AddRoute(Route route)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.CreateRouteEndpoint +"/"+ Globals.LoggedInUser.Id.ToString()
            };

            await genericRepository.PostAsync<Route>(builder.ToString(), route);
            
            //await Cache.InsertObject(CacheNameConstants.AllRoutes, routesList, DateTimeOffset.Now.AddSeconds(20));
            return route;
        }

        //Gets a route with all its tourist attraction list
        public async Task<Route> GetRouteAsync(Route route)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                List<Route> routesWithAttractionsFromCache = await GetFromCache<List<Route>>(CacheNameConstants.AllRoutes);
                Route routeReturn = routesWithAttractionsFromCache?.Where(c => c.Id == route.Id).FirstOrDefault();
                if (routesWithAttractionsFromCache != null && routeReturn != null && routeReturn.Attractions.Count != 0)//loaded from cache
                {
                    return routeReturn;
                }
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.RouteDetailsEndpoint + $"/{route.Id}"
            };

            var routeToReturn = await genericRepository.GetAsync<Route>(builder.ToString());

            return routeToReturn;
        }

        public async Task<Route> DeleteRoute(Route route)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.RoutesEndpoint + $"/{route.Id}"
            };

            await genericRepository.DeleteAsync(builder.ToString());
            return route;
        }

        public async Task<TouristAttraction> DeleteAttractionFromRoute(TouristAttraction attraction, int routeId)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.DeleteAttractionToRouteEndpoint + $"/{routeId}"+ $"/{attraction.Id}"
            };

            await genericRepository.DeleteAsync(builder.ToString());
            return attraction;
        }

        public async Task<Route> UpdateRoute(Route route)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.UpdateRouteEndpoint + $"/{route.Id}"
            };

            await genericRepository.PutAsync(builder.ToString(), route);
            return route;
        }

        //public async Task<IEnumerable<Route>> GetAllRoutesForUserWithAttractionsAsync()
        //{
        //    //List<Route> routesFromCache = await GetFromCache<List<Route>>(CacheNameConstants.AllRoutes);

        //    //if (routesFromCache != null)//loaded from cache
        //    //{
        //    //    return routesFromCache;
        //    //}
        //    //else
        //    //{
        //    UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
        //    {
        //        Path = ApiConstants.RoutesWithAttractionsEndpoint + "/" + Globals.LoggedInUser.Id.ToString()
        //    };

        //    var routes = await genericRepository.GetAsync<List<Route>>(builder.ToString());
        //    //await Cache.InsertObject(CacheNameConstants.AllRoutes, routes, DateTimeOffset.Now.AddSeconds(20));

        //    return routes;
        //    //}
        //}
    }
}
