﻿using Akavache;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Trip.Constants;
using Trip.Exceptions;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;
using Xamarin.Essentials;

namespace Trip.Services
{
    public class AuthenticationDataService : BaseService, IAuthenticationDataService
    {
        private readonly IGenericRepository genericRepository;
        
        public AuthenticationDataService(IGenericRepository genericRepo,
            IBlobCache cache = null) : base(cache)
        {
            this.genericRepository = genericRepo;
        }

        public async Task<AuthenticationResponse> AuthenticateAsync(string userName, string password)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                User userReturn = await GetFromCache<User>(CacheNameConstants.User);
                if (userReturn != null )//loaded from cache
                {
                    AuthenticationResponse response = new AuthenticationResponse()
                    {
                        IsAuthenticated = true,
                        User = userReturn
                    };

                    return response;
                }

                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AuthenticateEndpoint
            };

            AuthenticationRequest authenticationRequest = new AuthenticationRequest()
            {
                UserName = userName,
                Password = password
            };
            
            var responseToReturn = await genericRepository.PostAsync<AuthenticationRequest, AuthenticationResponse>(builder.ToString(), authenticationRequest);
            if(responseToReturn.IsAuthenticated!=false)
            {
                await Cache.InsertObject(CacheNameConstants.User, responseToReturn.User, DateTimeOffset.Now.AddSeconds(90000000000));
            }

            return responseToReturn;
        }

        public bool IsUserAuthenticated()
        {
            throw new NotImplementedException();
        }

        public async Task<AuthenticationResponse> RegisterAsync(string UserName, string Password, string FirstName, string LastName, string Email)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.None)
            {
                return null;
            }

            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.RegisterEndpoint
            };

            AuthenticationRequest authenticationRequest = new AuthenticationRequest()
            {
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                UserName = UserName,
                Password = Password
            };

            var responseToReturn = await genericRepository.PostAsync<AuthenticationRequest, AuthenticationResponse>(builder.ToString(), authenticationRequest);
            if (responseToReturn.IsAuthenticated != false && responseToReturn.User!=null)
            {
                await Cache.InsertObject(CacheNameConstants.User, responseToReturn.User, DateTimeOffset.Now.AddSeconds(90000000000));
            }
            return responseToReturn;
        }
    }

}
