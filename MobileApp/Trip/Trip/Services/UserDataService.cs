﻿using Akavache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trip.Interfaces;
using Trip.Interfaces.Data;
using Trip.Models;

namespace Trip.Services
{
    public class UserDataService : BaseService, IUserDataService
    {
        private readonly IGenericRepository _genericRepository;

        public UserDataService(IGenericRepository genericRepository,
            IBlobCache cache = null) : base(cache)
        {
            this._genericRepository = genericRepository;
        }
    }
}
