﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Trip.Interfaces;
using Trip.Models;

namespace Trip.Services.General
{
    public class DialogService : IDialogService
    {
        public Task ShowDialog(string message, string title, string buttonLabel)
        {
            return UserDialogs.Instance.AlertAsync(message, title, buttonLabel);
        }

        public void ShowToast(string message, Color color)
        {
            var toastConfig = new ToastConfig(message);
            toastConfig.SetDuration(5000);
            toastConfig.SetBackgroundColor(color);
            UserDialogs.Instance.Toast(toastConfig);
        }

        public Task<string> DisplayActionSheet(string title, string cancel, string destructive, CancellationToken? cancelToken = null, params string[] buttons)
        {
            return UserDialogs.Instance.ActionSheetAsync(title, cancel, destructive, cancelToken, buttons);
        }

    }
}
