﻿using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using Trip.Interfaces;

namespace Trip.Services.General
{
    public class PhoneService : IPhoneService
    {
        public void MakePhoneCall()
        {
            CrossMessaging.Current.PhoneDialer.MakePhoneCall("+40749874165");
        }

    }
}
