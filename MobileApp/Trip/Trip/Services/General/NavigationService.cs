﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Trip.Interfaces;
using Trip.ViewModels;
using Trip.Views;
using Xamarin.Forms;

namespace Trip.Services.General
{
    public class NavigationService : INavigationService
    {
        private readonly Dictionary<Type, Type> _mappings;
        protected Application CurrentApplication => Application.Current;
        
        public NavigationService()
        {
            _mappings = new Dictionary<Type, Type>();

            CreatePageViewModelMappings();
        }

        private void CreatePageViewModelMappings()
        {

            _mappings.Add(typeof(LoginViewModel), typeof(LoginView));
            _mappings.Add(typeof(MainViewModel), typeof(MainView));
            _mappings.Add(typeof(MenuViewModel), typeof(MenuView));
            _mappings.Add(typeof(CitiesViewModel), typeof(CitiesView));
            _mappings.Add(typeof(CityDetailViewModel), typeof(CityDetailView));
            _mappings.Add(typeof(ProfileViewModel), typeof(ProfileView));
            _mappings.Add(typeof(TouristAttractionViewModel), typeof(TouristAttractionView));
            _mappings.Add(typeof(RouteDetailViewModel), typeof(RouteDetailView));
            _mappings.Add(typeof(RoutesViewModel), typeof(RoutesView));
            _mappings.Add(typeof(RegistrationViewModel), typeof(RegistrationView));
            _mappings.Add(typeof(MapViewModel), typeof(MapView));
            _mappings.Add(typeof(ContactViewModel), typeof(ContactView));
            _mappings.Add(typeof(CreateRouteViewModel), typeof(CreateRouteView));
            _mappings.Add(typeof(CreateAttractionViewModel), typeof(CreateAttractionView));
        }

        public async Task InitializeAsync(bool isPushToStack=true)
        {
            if (Globals.LoggedInUser == null)
            {
                await NavigateToAsync<LoginViewModel>();
            }
            else
            {
                await NavigateToAsync<MainViewModel>();
            }
        }

        public async Task ClearBackStack()
        {
            await CurrentApplication.MainPage.Navigation.PopToRootAsync();
        }

        public async Task NavigateBackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.Detail.Navigation.PopAsync();
            }
            else if (CurrentApplication.MainPage != null)
            {
                await CurrentApplication.MainPage.Navigation.PopAsync();
            }
        }


        public virtual Task RemoveLastFromBackStackAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                mainPage.Detail.Navigation.RemovePage(
                    mainPage.Detail.Navigation.NavigationStack[mainPage.Detail.Navigation.NavigationStack.Count - 2]);
            }

            return Task.FromResult(true);
        }

        public async Task PopToRootAsync()
        {
            if (CurrentApplication.MainPage is MainView mainPage)
            {
                await mainPage.Detail.Navigation.PopToRootAsync();
            }
        }
        public Task NavigateToAsync<TViewModel>(object parameter1, object parameter2, bool isPushToStack = true) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter1, parameter2, isPushToStack);
        }

        public Task NavigateToAsync<TViewModel>(bool isPushToStack = true) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), null, null, isPushToStack);
        }

        public Task NavigateToAsync<TViewModel>(object parameter, bool isPushToStack = true) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter, null, isPushToStack);
        }

        public Task NavigateToAsync(Type viewModelType, bool isPushToStack = true)
        {
            return InternalNavigateToAsync(viewModelType, null,null, isPushToStack );
        }

        public Task NavigateToAsync(Type viewModelType, object parameter, bool isPushToStack = true)
        {
            return InternalNavigateToAsync(viewModelType, parameter, null, isPushToStack);
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter1, object parameter2, bool isPushToStack= true)
        {
            Page page = CreatePage(viewModelType);

            if (page is MainView || page is RegistrationView)
            {
                CurrentApplication.MainPage = page;
            }
            else if (page is LoginView)
            {
                CurrentApplication.MainPage = page;
            }
            else if (CurrentApplication.MainPage is MainView)
            {
                var mainPage = CurrentApplication.MainPage as MainView;
                if (mainPage.Detail is CustomNavigationView navigationPage)
                {
                    var currentPage = navigationPage.CurrentPage;

                    if (currentPage.GetType() != page.GetType())
                    {
                        if(isPushToStack)
                        {
                            await navigationPage.PushAsync(page);
                        }
                    }
                }
                else
                {
                    navigationPage = new CustomNavigationView(page);
                    mainPage.Detail = navigationPage;
                }

                mainPage.IsPresented = false;
            }
            else
            {
                var navigationPage = CurrentApplication.MainPage as CustomNavigationView;

                if (navigationPage != null)
                {
                    await navigationPage.PushAsync(page);
                }
                else
                {
                    CurrentApplication.MainPage = new CustomNavigationView(page);
                }
            }

            await (page.BindingContext as BaseViewModel).InitializeAsync(parameter1,parameter2);
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            if (!_mappings.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for ${viewModelType} was found on navigation mappings");
            }

            return _mappings[viewModelType];
        }

        private Page CreatePage(Type viewModelType)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            return page;
        }
    }
}
